﻿using System.Collections;
using System.Collections.Generic;

namespace TMASimulator.Models
{
    public class VesselTypeSet : IEnumerable<VesselType>
    {
        private readonly List<VesselType> _vesselTypeList = new List<VesselType>();
        public void Add(VesselType vesselType) //command
        {
            _vesselTypeList.Add(vesselType);

        }

        public int Count() //query
        {
            return _vesselTypeList.Count;
        }

        public VesselType GetById(int id)  //query
        {
            //TODO: Duplicated code with TMASimulationRun.getVesselByID() - Refactor
            var result = _vesselTypeList.Find(
                theVessel => theVessel.VesselTypeId == id
                );

            if (result != null)
            {
                return result;
            }
            // ReSharper disable once RedundantIfElseBlock
            else
            {
                throw new KeyNotFoundException("VesselTypeID Not found");
            }
        }

        public VesselType GetByIndex(int index) //query
        {
            return _vesselTypeList[index];
        }

        public List<VesselType> GetList()  //query
        {
            return _vesselTypeList;
        }

        public void RemoveByIndex(int index)  //command
        {
            _vesselTypeList.RemoveAt(index);
        }

        public void RemoveById(int id)  //command
        {
            _vesselTypeList.Remove(_vesselTypeList.Find(theVesselType => theVesselType.VesselTypeId == id));
        }

        IEnumerator<VesselType> IEnumerable<VesselType>.GetEnumerator()
        {
            return _vesselTypeList.GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}