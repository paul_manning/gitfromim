﻿
namespace TMASimulator.Models
{
    public class VesselType
    {
        private readonly int _vesselTypeId;
        private readonly MovementCharacteristics _movementCharacteristics;
        public string VesselTypeName { get; set; }

        public VesselType(int id, string vesselTypeName)
        {
            _vesselTypeId = id;
            VesselTypeName = vesselTypeName;
            _movementCharacteristics = new MovementCharacteristics();
        }

        public VesselType(int id, string vesselTypeName, MovementCharacteristics mc)
        {
            _movementCharacteristics = mc;
            _vesselTypeId = id;
            VesselTypeName = vesselTypeName;
        }
        public int VesselTypeId
        {
            get { return _vesselTypeId; }
        }

        public MovementCharacteristics Limits()
        {
            return _movementCharacteristics;
        }
    }
}