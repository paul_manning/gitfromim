﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TMASimulator.Models
{
    public class VesselSet :IEnumerable<Vessel>
    {
        private readonly List<Vessel> _vessels = new List<Vessel>();
        
         public void AddVessel(Vessel theVessel) //command
        {
            _vessels.Add(theVessel);
        }

        public int GetVesselCount() //query
        {
            return _vessels.Count;
        }

        public void RemoveVessel(int id) //command
        { 
            _vessels.Remove(_vessels.First(theVessel => theVessel.VesselId == id));
        }

        public List<Vessel> GetAllVessels() //query
        {
            return _vessels;
        }

        public Vessel GetVesselByIndex(int index)  //query
        {
            return _vessels[index];
        }

        public Vessel GetVesselById(int vesselId)  //query
        {
            var result =  _vessels.Find(
                theVessel => theVessel.VesselId == vesselId
                );
            
            if (result != null)
            {
                return result;
            }
            // ReSharper disable once RedundantIfElseBlock
            else
            {
                throw new KeyNotFoundException("VesselID Not found");
            }

  
        }

        public void ReplaceVessel(int index, Vessel newVessel) //command
        {
            _vessels[index] = newVessel;
        }

        public void MoveVesselItem(int oldIndex, int newIndex) //command
        {
            var item = _vessels[oldIndex];
            _vessels.RemoveAt(oldIndex);
            _vessels.Insert(newIndex, item);

        }

        public IEnumerator<Vessel> GetEnumerator()
        {
            return _vessels.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}