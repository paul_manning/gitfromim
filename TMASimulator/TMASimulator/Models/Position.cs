﻿using System;
using System.Device.Location;

namespace TMASimulator.Models
{
    public class Position
    {
        private bool _noWrap;
        private double _longitude;
        private double _latitude;
        private bool _isValid;
        private readonly GeoCoordinate _location = new GeoCoordinate();
        public Position() //ctor
        {
            _location = new GeoCoordinate();
            _noWrap = false;
            _isValid = false;
        }

        public Position(double lat, double lon, bool noWrap) //ctor
        {
            _noWrap = noWrap;
            Latitude = lat;
            Longitude = lon;
            _location = new GeoCoordinate(Latitude, Longitude);
        }
        public Position(double lat, double lon)  //ctor
        {
            _noWrap = false;
            Latitude = lat;
            Longitude = lon;
            _location = new GeoCoordinate(Latitude, Longitude);
        }

        public Position GetNewPosition(TimeSpan t, Movement m) //query
        {
            const double radiusEarthKilometres = 6371.01;
            var distRatio = (m.GetDistanceTraveledMeters(t)/1000) / radiusEarthKilometres;
            var distRatioSine = Math.Sin(distRatio);
            var distRatioCosine = Math.Cos(distRatio);

            var startLatRad = ToRad(_location.Latitude);
            var startLonRad = ToRad(_location.Longitude);

            var startLatCos = Math.Cos(startLatRad);
            var startLatSin = Math.Sin(startLatRad);
            var initialBearingRadians = ToRad(m.Course);
            var endLatRads = Math.Asin((startLatSin * distRatioCosine) + (startLatCos * distRatioSine * Math.Cos(initialBearingRadians)));

            var endLonRads = startLonRad
                + Math.Atan2(
                    Math.Sin(initialBearingRadians) * distRatioSine * startLatCos,
                    distRatioCosine - startLatSin * Math.Sin(endLatRads));

            var newLatitude = ToDegrees(endLatRads);
            var newLongitude = ToDegrees(endLonRads);
           return new Position(newLatitude, newLongitude);     
        }

        public double BearingTo(Position p2)  //query
        {
            return GetBearing(Latitude, Longitude, p2.Latitude, p2.Longitude);
        }

        private static double GetBearing(double lat1, double lon1, double lat2, double lon2) //query
        {
            //const double R = 6371; //earth’s radius (mean radius = 6,371km)
            var dLon = ToRad(lon2 - lon1);
            var dPhi = Math.Log(
                Math.Tan(ToRad(lat2)/2 + Math.PI/4)/Math.Tan(ToRad(lat1)/2 + Math.PI/4));
            if (Math.Abs(dLon) > Math.PI)
                dLon = dLon > 0 ? -(2*Math.PI - dLon) : (2*Math.PI + dLon);
            return ToBearing(Math.Atan2(dLon, dPhi));
        }

        private static double ToRad(double degrees)  //query
        {
            return degrees*(Math.PI/180);
        }

        private static double ToDegrees(double radians)  //query
        {
            return radians*180/Math.PI;
        }

        private static double ToBearing(double radians)  //query
        {
            // convert radians to degrees (as bearing: 0...360)
            return (ToDegrees(radians) + 360)%360;
        }

        public double Longitude //prop
        {
            get { return _longitude; }
            set
            {
                _longitude = _noWrap?value:value%180;
                _location.Longitude = _longitude;
                _isValid = (_longitude < 180 && _longitude > -180);
            }
        }

        public double Latitude  //prop
        {
            get { return _latitude; }
            set
            {
                _latitude = _noWrap ? value : value%90;
                _location.Latitude = _latitude;
                _isValid = (_latitude < 90 && _latitude > -90);
            }
        }

        public override bool Equals(object obj) //query
        {
            if (obj.GetType().Equals(typeof (Position)))
            {
                Position p = (Position) obj;
                return _location.Equals(p._location);
           
            }
                 return _location.Equals(obj);
        }

        public override int GetHashCode() //query
        {
            return _location.GetHashCode();
        }

        public override string ToString()  //query
        {
            return _location.ToString();
        }

        public double GetDistanceTo(Position p)  //query
        {
            return _location.GetDistanceTo(p._location);
        }
        public bool NoWrap  //prop
        {
            get { return _noWrap; }
            set { _noWrap = value; }
        }
        public bool IsValid()  //query
        {
            //Lat -90 to +90
            //Lon -180 to 180
            return _isValid;

        }
        
    }
}