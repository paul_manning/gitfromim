namespace TMASimulator.Models.Orders
{
    public class RelativeDepthChangeUpOrder:MovementOrderBase
    {
        public RelativeDepthChangeUpOrder(int startTime, double orderValue, MovementCharacteristics limits)
            : base(startTime, orderValue, limits)
        {
            DirectionMultiplier = -1;
            //    OrderValue = (DirectionMultiplier * orderValue) + currentValue;
            ApplyLimits(Limits.MinDepth, Limits.MaxDepth);

        }

        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Depth;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Relative;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new RelativeDepthChangeUpOrder(StartTimeInSeconds, OrderValue, Limits);
        }

    }
}