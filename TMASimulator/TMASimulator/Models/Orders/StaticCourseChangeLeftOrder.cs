namespace TMASimulator.Models.Orders
{
    public class StaticCourseChangeLeftOrder: MovementOrderBase
    {
        public StaticCourseChangeLeftOrder(int startTime, double orderValue, MovementCharacteristics limits)
            : base(startTime, orderValue, limits)
        {
            DirectionMultiplier = -1;
            OrderValue = CleanCourse(orderValue);

        }

        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Course;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Static;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new StaticCourseChangeLeftOrder( StartTimeInSeconds, OrderValue, Limits);
        }
    }
}