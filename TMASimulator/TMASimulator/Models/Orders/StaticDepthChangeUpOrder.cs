namespace TMASimulator.Models.Orders
{
    public class StaticDepthChangeUpOrder:MovementOrderBase
    {
        public StaticDepthChangeUpOrder(int startTime, double orderValue, MovementCharacteristics limits)
            : base(startTime, orderValue, limits)
        {
            DirectionMultiplier = -1;
            ApplyLimits(Limits.MinDepth, Limits.MaxDepth);

        }

        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Depth;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Static;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new StaticDepthChangeUpOrder(StartTimeInSeconds, OrderValue, Limits);
        }
    }
}