using System;

namespace TMASimulator.Models.Orders
{
    public  class MovementOrderBase
    {
        public int StartTimeInSeconds { get; protected internal set; }

        public double OrderValue { get; protected set; }
        public int DirectionMultiplier { get; protected set; }
        public bool HasExecuted { get; protected internal set; }
        public MovementCharacteristics Limits { get; protected set; }


        protected MovementOrderBase(
            int startTime, 
            double orderValue, 
            MovementCharacteristics limits)
        {
            StartTimeInSeconds = startTime;
            OrderValue = orderValue;
            Limits = limits;
            DirectionMultiplier =0;
        }

        public virtual TypeOfChangeEnum TypeOfChange()
        {
            throw new NotImplementedException();
        }

        public virtual RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            throw new NotImplementedException();
        }
        protected void ApplyLimits(double min, double max)
        {
            OrderValue = OrderValue > max ? max : OrderValue;
            OrderValue = OrderValue < min ? min : OrderValue;
        }


        protected static double CleanCourse(double value)
        {
            if (value >= 360)
            {
                return value % 360;
            }
            if (value < 0)
            {
                return 360 - Math.Abs(value) % 360;
            }
            return value;
        }

        public virtual MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            throw new NotImplementedException();
        }
    }
}