namespace TMASimulator.Models.Orders
{
    public enum TypeOfChangeEnum
    {
        Course, Speed, Depth
    }
}