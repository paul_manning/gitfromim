namespace TMASimulator.Models.Orders
{
    public class RelativeSpeedChangeDecreaseOrder:MovementOrderBase
    {
        public RelativeSpeedChangeDecreaseOrder(int startTime, double orderValue, MovementCharacteristics limits) : base(startTime, orderValue, limits)
        {
         DirectionMultiplier = -1;
//       OrderValue = (DirectionMultiplier * orderValue) + currentValue;
         ApplyLimits(Limits.MinSpeed, Limits.MaxSpeed);
        }

        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Speed;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Relative;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new RelativeSpeedChangeDecreaseOrder(StartTimeInSeconds, OrderValue, Limits);
        }
    }
}