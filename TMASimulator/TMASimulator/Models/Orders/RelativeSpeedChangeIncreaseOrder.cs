namespace TMASimulator.Models.Orders
{
    public class RelativeSpeedChangeIncreaseOrder:MovementOrderBase
    {
        public RelativeSpeedChangeIncreaseOrder(int startTime, double orderValue, MovementCharacteristics limits) : base(startTime, orderValue, limits)
        {
            DirectionMultiplier = 1;
            //    OrderValue = (DirectionMultiplier * orderValue) + currentValue;
            ApplyLimits(Limits.MinSpeed, Limits.MaxSpeed);

        }
        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Speed;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Relative;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new RelativeSpeedChangeIncreaseOrder( StartTimeInSeconds, OrderValue, Limits);
        }
    }
}