namespace TMASimulator.Models.Orders
{
    public class RelativeDepthChangeDownOrder:MovementOrderBase
    {
        public RelativeDepthChangeDownOrder(int startTime, double orderValue, MovementCharacteristics limits) : base(startTime, orderValue, limits)
        {
            //todo - Depth should be negative if below the surface, and positive if airborne
            //todo - fix this assumption on direction.  
            DirectionMultiplier = 1;
            ApplyLimits(limits.MinDepth, limits.MaxDepth);
        }

        public override TypeOfChangeEnum TypeOfChange()
        {
            return TypeOfChangeEnum.Depth;
        }

        public override RelativeStaticEnum TypeOfChangeRelativeStatic()
        {
            return RelativeStaticEnum.Relative;
        }

        public override MovementOrderBase GetNewOfMeFromCurrentOrder()
        {
            return new RelativeDepthChangeDownOrder( StartTimeInSeconds, OrderValue, Limits);
        }
    }
}