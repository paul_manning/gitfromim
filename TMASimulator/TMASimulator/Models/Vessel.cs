﻿namespace TMASimulator.Models
{
    public class Vessel
    {
        private readonly int _vesselId;
        private VesselType _vesselType;

        public Position Position { get; set; }

        public Vessel(int id)
        {
            _vesselId = id;
            _vesselType = new VesselType(-1, "no type");
        } 

        public Vessel(int id, VesselType vt)
        {
            _vesselId = id;
            _vesselType = vt;
        }
        public int VesselId
        {
            get { return _vesselId; }
        }

        public VesselType VesselType
        {
            get { return _vesselType; }
            set { _vesselType = value; }
        }
        
    }

}