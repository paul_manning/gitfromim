﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Ajax.Utilities;
using TMASimulator.Models.Orders;

namespace TMASimulator.Models
{
    public class MovementOrderSet : ICollection<MovementOrderBase>, IEnumerable<MovementOrderBase>
    {
        private List<MovementOrderBase> _orders;

        public MovementOrderSet(string orderSetName)
        {
            OrderSetName = orderSetName;
            _orders = new List<MovementOrderBase>();
        }

        private string OrderSetName { get; set; }


        public void Add(MovementOrderBase order)
        {
            if (Exists(order.StartTimeInSeconds, order.TypeOfChange()))
                throw new Exception("Attempted To Add an item to a list that already exists (iime, ordertype)");
            _orders.Add(order);
        }

        public void Clear()
        {
            _orders.Clear();
        }

        public bool Contains(MovementOrderBase item)
        {
            return _orders.Contains(item);
        }

        public void CopyTo(MovementOrderBase[] array, int arrayIndex)
        {
            _orders.CopyTo(array, arrayIndex);
        }

        public bool Remove(MovementOrderBase item)
        {
            return _orders.Remove(item);
        }

        int ICollection<MovementOrderBase>.Count
        {
            get { return _orders.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public IEnumerator<MovementOrderBase> GetEnumerator()
        {
            return _orders.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _orders).GetEnumerator();
        }

        public void MarkComplete(int time)
        {
            List<MovementOrderBase> result = _orders.FindAll(item => item.StartTimeInSeconds == time);
            foreach (MovementOrderBase item in result)
            {
                item.HasExecuted = true;
            }
        }

        public int Count()
        {
            return _orders.Count();
        }

        public List<MovementOrderBase> GetList()
        {
            return new List<MovementOrderBase>(_orders.OrderBy(item => item.StartTimeInSeconds));
        }

        public bool IsCompleted(int time)
        {
            List<MovementOrderBase> result =
                _orders.FindAll(item => (item.StartTimeInSeconds == time) && item.HasExecuted);
            return result.Count() != 0;
        }

        public int Count(int time)
        {
            int result = _orders.FindAll(item => item.StartTimeInSeconds == time).Count();
            return result;
        }

        public void InsertSetReplaceEnd(MovementOrderSet newSet, int insertTime)
        {
            List<MovementOrderBase> resultSet = _orders.FindAll(o => o.StartTimeInSeconds < insertTime);
            resultSet.AddRange(newSet.GetList());
            _orders = resultSet;
        }


        public void Replace(MovementOrderBase newOrder, List<MovementOrderBase> theList)
        {
            if (
                !theList.Exists(
                    o =>
                        o.StartTimeInSeconds == newOrder.StartTimeInSeconds &&
                        o.TypeOfChange() == newOrder.TypeOfChange()))
            {
                theList.Add(newOrder);
            }
            else
            {
                theList.Remove(
                    theList.Find(
                        item =>
                            item.StartTimeInSeconds == newOrder.StartTimeInSeconds &&
                            item.TypeOfChange() == newOrder.TypeOfChange()));
                theList.Add(newOrder);
            }
        }

        public void InsertSet(MovementOrderSet ms2)
        {
            InsertSet(ms2, 0);
        }

        public void InsertSetAtEnd(MovementOrderSet ms2)
        {
            int ms2Endtime = _orders.Last().StartTimeInSeconds + 1;
            InsertSet(ms2, ms2Endtime);
        }

        public void InsertSet(MovementOrderSet ms2, int insertTime)
        {
            /* Flow:
             * Get the time offset from the first start time of ms2
             * get the size in time of ms2 (new function?)
             * OldSet_insertOffset = insertTime + newset size 
             * set all the items in _orders with a start time >= inserttime to be their time + oldset_insertOffset
             * set all the items in ms2 with a start time + insertTime + 1
             * add the ms2 items to _orders
             * 
             * example: 
             *  old set has 1 item, time 0
             *  new set 1 has 1 item, time 0
             *  new set 2 has 5 items, starting time 0
             *  insert at time 0, set 1, old first item time now should be 1
             *  insert at time 0, set 2, old first should be at time 6
             */
            var newSetEndtime = ms2.GetList().Last().StartTimeInSeconds; //4
            var orderInsertOffset = insertTime + newSetEndtime + 1;  //5
            //ignore the items before insertTime
            var ordersGtInsertTime = _orders.Where
                (o => o.StartTimeInSeconds >= insertTime);  //1 item
            foreach (var item in ordersGtInsertTime)
            {
                //should move start time of old set to new positon
                item.StartTimeInSeconds = 
                    item.StartTimeInSeconds + orderInsertOffset; //5
            }
            foreach (var item in ms2.GetList())
            {
                //should add in new set starting at original value + inserttime
                item.StartTimeInSeconds = 
                    item.StartTimeInSeconds + insertTime; //0, 1, 2, 3, 4,
                Add(item); //ok
            }
        }


        public void Replace( MovementOrderBase newOrder)
        {
            Replace(newOrder, _orders);
        }

        public MovementOrderBase Get(int time, TypeOfChangeEnum orderType)
        {
            if (Exists(time, orderType))
                return _orders.FindAll(o => o.StartTimeInSeconds == time && o.TypeOfChange() == orderType).First();
            throw new ArgumentOutOfRangeException("Attempted to get an item from list that doesn't exist");
        }

        public bool Exists(int time, TypeOfChangeEnum orderType)
        {
            return _orders.Exists(item => item.StartTimeInSeconds == time && item.TypeOfChange() == orderType);
        }


        public void Remove(int timeToRemove, TypeOfChangeEnum typeOfChange)
        {
            //todo  shift orders up? 
            MovementOrderBase itemToRemove = _orders
                .First(o => o.TypeOfChange() == typeOfChange
                            && o.StartTimeInSeconds == timeToRemove);
            _orders.Remove(itemToRemove);
        }

        public MovementOrderBase GetCurrentOrder(int timeSegment, TypeOfChangeEnum typeOfChange)
        {
            MovementOrderBase lastItem = GetList().Last(o => o.TypeOfChange() == typeOfChange);
            return timeSegment > lastItem.StartTimeInSeconds
                ? lastItem
                : _orders.Where(o => o.StartTimeInSeconds <= timeSegment
                                     && o.TypeOfChange() == typeOfChange)
                    .OrderBy(o => o.StartTimeInSeconds).Last();
        }

        public IEnumerable<MovementOrderBase> GetCurrentOrders(int timeSegment)
        {
            var result = new List<MovementOrderBase>();
            if (timeSegment <= this.First().StartTimeInSeconds) 
                throw new Exception("attempt to get an order before being set");

            var lastCourse = GetCurrentOrder(timeSegment, TypeOfChangeEnum.Course);
            var lastDepth  = GetCurrentOrder(timeSegment, TypeOfChangeEnum.Depth);
            var lastSpeed  = GetCurrentOrder(timeSegment, TypeOfChangeEnum.Speed);
            
            if (lastCourse != null) result.Add(lastCourse);
            if (lastSpeed  != null) result.Add(lastSpeed);
            if (lastDepth  != null) result.Add(lastDepth);

            return result;
        }

        public MovementOrderBase GetNextOrder(int timeSegment, TypeOfChangeEnum changetype)
        {
            var result = _orders
                .OrderBy(o => o.StartTimeInSeconds)
                .First(o => o.TypeOfChange() == changetype
                    && o.StartTimeInSeconds > timeSegment);
            return result;
        }
    }
}