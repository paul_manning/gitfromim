﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TMASimulator.Models;

namespace TMASimulator.Tests
{
    [TestClass]
// ReSharper disable once InconsistentNaming
    public class TMASimulation_VesselTypeTests
    {
        [TestMethod]
        public void VesselType_ShouldHaveMovementCharacteristics()
        {
            var smc = new MovementCharacteristics();
            smc.MinSpeed = -15;
            smc.MaxSpeed = 25;
            smc.MinDepth = 0;
            smc.MaxDepth = 0;
            smc.SpeedChangeRate = 1; //k/s
            smc.DepthChangeRate = 0; //m/s
            smc.CourseChangeRate = 1; //d/s

            var vt = new VesselType(1, "VesselTypeName", smc);
            Assert.AreEqual(-15, vt.Limits().MinSpeed);
            Assert.AreEqual(true, vt.Limits().HasBeenSet);

            var vt2 = new VesselType(2, "No default SMC");
            Assert.AreEqual(0, vt2.Limits().MaxSpeed, "setting no movementcharacterisitcs should return 0 on items");
            Assert.AreEqual(false, vt2.Limits().HasBeenSet, "not setting should indicate so");
        }


        [TestMethod]
        public void Vessel_ShouldBeAbleToBeLinkedToAVesselType()
        {
            var theVesselType = new VesselType(1, "FancyVesselType");
            var theVessel = new Vessel(1, theVesselType);
            Assert.AreEqual("FancyVesselType", theVessel.VesselType.VesselTypeName);
        }

        [TestMethod]
        public void Vessel_ShouldBeAbleToBeReLinkedToAnotherVesselType()
        {
            var theFirstVesselType = new VesselType(1, "First VesselType");
            var theSecondVesselType = new VesselType(2, "Second VesselType");

            // ReSharper disable once UseObjectOrCollectionInitializer
            var theVessel = new Vessel(1, theFirstVesselType);
            theVessel.VesselType = theSecondVesselType;
            Assert.AreEqual("Second VesselType", theVessel.VesselType.VesselTypeName, "Unexpected Vessel Type");
        }

        [TestMethod]
        public void Vessel_ShouldBeAbleToGetItsVesselType()
        {
            var theVessel = new Vessel(1, new VesselType(1, "VesselTypeName"));
            VesselType resultVesselType = theVessel.VesselType;
            Assert.AreEqual("VesselTypeName", resultVesselType.VesselTypeName);
        }

        [TestMethod]
        public void VesselTypeList_ShouldBeAbleToAddAVesselType()
        {
            const int expectedCount = 1;
            var vtl = new VesselTypeSet();
            var vt = new VesselType(1, "VesselTypeName");

            vtl.Add(vt);
            Assert.AreEqual(expectedCount, vtl.Count());
        }

        [TestMethod]
        public void VesselTypeList_ShouldBeAbleToGetASingleVesselTypeById()
        {
            var vtl = new VesselTypeSet();
            const int numberOfVesselTypesToAdd = 5;
            const string expectedResultString = "VesselType 3";

            FillVesselTypes(numberOfVesselTypesToAdd, vtl);
            VesselType result = vtl.GetById(3);
            Assert.AreEqual(expectedResultString, result.VesselTypeName);
        }

        private static void FillVesselTypes(int numberOfVesselTypesToAdd, VesselTypeSet vtl)
        {
            for (int vtid = 0; vtid < numberOfVesselTypesToAdd; vtid++)
            {
                vtl.Add(new VesselType(vtid, string.Format("VesselType {0}", vtid)));
            }
        }

        [TestMethod]
        public void VesselTypeList_ShouldBeAbleToGetASingleVesselTypeByIndex()
        {
            var vtl = new VesselTypeSet();
            const int numberOfVesselTypesToAdd = 5;
            const string expectedResultString = "VesselType 2";
            FillVesselTypes(numberOfVesselTypesToAdd, vtl);

            VesselType result = vtl.GetByIndex(2);
            Assert.AreEqual(expectedResultString, result.VesselTypeName);
        }

        [TestMethod]
        public void VesselTypeList_ShouldBeAbleToGetAListOfVesselTypes()
        {
            var vtl = new VesselTypeSet();
            const int numberOfVesselTypesToAdd = 5;
            FillVesselTypes(numberOfVesselTypesToAdd, vtl);

            List<VesselType> resultList = vtl.GetList();

            Assert.AreEqual(numberOfVesselTypesToAdd, resultList.Count);
        }

        [TestMethod]
        public void VesselTypeList_ShouldBeAbleToRemoveAVesselTypeByIndex()
        {
            var vtl = new VesselTypeSet();
            const int numberOfVesselTypesToAdd = 5;
            int[] expectedResult = {0, 1, 3, 4};
            FillVesselTypes(numberOfVesselTypesToAdd, vtl);
            Assert.AreEqual(numberOfVesselTypesToAdd, vtl.Count());

            vtl.RemoveByIndex(2);
            Assert.AreEqual(4, vtl.Count());
            VerifyArrayValues(expectedResult, vtl);
        }

        private static void VerifyArrayValues(int[] expectedResult, VesselTypeSet vtl)
        {
            for (int vindex = 0; vindex < expectedResult.Count(); vindex++)
            {
                Assert.AreEqual(expectedResult[vindex], vtl.GetByIndex(vindex).VesselTypeId,
                    string.Format("Unexpectedresult after removing VesselType from list - expected {0} Received {1}",
                        expectedResult, vtl.GetList().ToArray()));
            }
        }

        [TestMethod]
        public void VesselTypeList_ShouldBeAbleToRemoveAVesselTypeById()
        {
            var vtl = new VesselTypeSet();
            const int numberOfVesselsToAdd = 5;
            int[] expectedResult = {0, 1, 3, 4};
            FillVesselTypes(numberOfVesselsToAdd, vtl);
            Assert.AreEqual(numberOfVesselsToAdd, vtl.Count());

            vtl.RemoveById(2);
            Assert.AreEqual(expectedResult.Count(), vtl.Count());
            VerifyArrayValues(expectedResult, vtl);
        }
    }
}