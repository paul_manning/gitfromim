﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TMASimulator.Models;
using TMASimulator.Models.Orders;

namespace TMASimulator.Tests
{
    [TestClass]
    public class MovementTests
    {
        [TestMethod]
        public void Movement_shouldreturnUpdatedMovement_GivenStaticCourseChangeOrder()
        {
            var m = new Movement(0, 10, 0);
            var t = new TimeSpan(0, 0, 1); //1 second
            var t2 = new TimeSpan(0, 0, 5);
            var mc = new MovementCharacteristics {CourseChangeRate = 1, MaxSpeed = 10, MinSpeed = -10};
            var order = new StaticCourseChangeLeftOrder(0, 270, mc);
            
            //prior to the order, the movement returned should be marked as not changing
            Assert.AreEqual(false, m.IsChanging);

            //once order is applied, it should mark isChanging to true
            Movement nextMovement = m.NextMovement(t, mc, order);

            Assert.AreEqual(359, nextMovement.Course, "Should only change one degree in a second");

            Assert.AreEqual(true, nextMovement.IsChanging);
                //returned movemment should be marked as isChanging unless the resulting movement = the ordered course/spped/depth
            Assert.AreEqual(false, m.IsChanging); //original movement should still be not changed

            var order2 = new StaticCourseChangeRightOrder(0, 2, mc);
            Movement anotherMovement = m.NextMovement(t2, mc, order2);
                //in 5 secs.  Should move only 2 degrees and have isChanging set to false

            Assert.AreEqual(2, anotherMovement.Course, "should only move 2 degrees");
            Assert.AreEqual(false, anotherMovement.IsChanging, "should be settled on course with this.");
        }
        [Ignore]
        [TestMethod]
        public void Movement_shouldReturnUpdatedMovement_GivenRelativeCourseChangeOrder()
        {
            //var m = new Movement(0, 10, 0);
            //var t = new TimeSpan(0, 0, 1);
            //var t2 = new TimeSpan(0, 0, 5);
            //var mc = new MovementCharacteristics {CourseChangeRate = 2, MaxSpeed = 10, MinSpeed = -5};
            //var order = new MovementOrder();
            //order.ChangeCourseRelative(0, Movement.Left, 15, m.Course);
            //Assert.AreEqual(true, order.IsCourseChange, "Should signify a course change");
            //Assert.IsFalse(m.IsChanging);

            //Movement nextMovement = m.NextMovement(t, mc, order); //left 2 degrees in one second

            //Assert.AreEqual(358, nextMovement.Course, "should turn 2 degrees left");
            //Assert.IsTrue(nextMovement.IsChanging, "Should still be changing course after this executes");
            //Assert.IsFalse(m.IsChanging, "No change expected here");

            //var secondOrder = new MovementOrder();
            //secondOrder.ChangeCourseRelative(0, Movement.Right, 8, m.Course);
            //Assert.IsTrue(order.IsCourseChange);

            //Movement secondMovement = m.NextMovement(t2, mc, secondOrder);
            //Assert.AreEqual(8, secondMovement.Course, "Should turn 8 degrees right");
            //Assert.IsFalse(secondMovement.IsChanging); //Should have settled
        }
        
        [TestMethod]
        public void Movement_shouldReturnUpdatedMovement_GivenStaticSpeedChangeOrder()
        {
            var m = new Movement(0, 10, 0);
            var t = new TimeSpan(0, 0, 5);
            var t2 = new TimeSpan(0, 0, 1);
            var mc = new MovementCharacteristics
            {
                CourseChangeRate = 2,
                SpeedChangeRate = 2,
                MaxSpeed = 20,
                MinSpeed = -5
            };
            var order = new StaticSpeedChangeDecreaseOrder(0, 5, mc);
//            order.ChangeSpeedTo(0, 5, mc);
            //Assert.IsTrue(order.IsSpeedChange, "Should indicate a speed change order");
            Assert.IsFalse(m.IsChanging, "Not changing yet.");

            Movement nextMovement = m.NextMovement(t, mc, order);

            Assert.AreEqual(5, nextMovement.Speed, "expected the change to complete fully");
            Assert.IsFalse(nextMovement.IsChanging, "Speed change should be completed");
            Assert.IsFalse(m.IsChanging, "should not have thouched the original movement");

            var nextOrder = new StaticSpeedChangeIncreaseOrder(0, 20, mc);
            //nextOrder.ChangeSpeedTo(0, 20, mc);
            Movement nextMovement2 = m.NextMovement(t2, mc, nextOrder);
            Assert.IsTrue(nextMovement2.IsChanging, "Should be mid-change");
            Assert.AreEqual(12, nextMovement2.Speed, "Should have increased 2 knowts");
        }
        [Ignore]
        [TestMethod]
        public void Movement_shouldReturnUpdatedMovement_GivenRelativeSpeedChangeOrder()
        {
            //var m = new Movement(0, 0, 0);
            //var t = new TimeSpan(0, 0, 1);
            //var t2 = new TimeSpan(0, 0, 5);
            //var mc = new MovementCharacteristics
            //{
            //    CourseChangeRate = 2,
            //    SpeedChangeRate = 2,
            //    MaxSpeed = 20,
            //    MinSpeed = -5
            //};
            //var order = new MovementOrder();

            //order.ChangeSpeedRelative(0, ChangeType.Increase, 5, m.Speed, mc);
            //Assert.IsFalse(m.IsChanging, "Not changing current movement");
            //Assert.IsTrue(order.IsSpeedChange, "is a Speed Change");

            //Movement m2 = m.NextMovement(t, mc, order);
            //Assert.AreEqual(2, m2.Speed, "Only time for two knot increase");
            //Assert.IsTrue(m2.IsChanging, "Mid-change");

            //var order2 = new MovementOrder();
            //order2.ChangeSpeedRelative(0, ChangeType.Increase, 5, m.Speed, mc);
            //Movement m3 = m.NextMovement(t2, mc, order2);

            //Assert.IsFalse(m3.IsChanging, "should be done changing");
            //Assert.AreEqual(5, m3.Speed, "Speed should be 5 knots");
        }
       
        [TestMethod]
        public void Movement_shouldReturnUpdatedMovement_GivenStaticDepthChangeOrder()
        {
            var m = new Movement(0, 0, 10);
            var t = new TimeSpan(0, 0, 1);
            var t2 = new TimeSpan(0, 0, 5);
            var mc = new MovementCharacteristics {DepthChangeRate = 1, MaxDepth = 20, MinDepth = 0};

            var order = new StaticDepthChangeDownOrder(0, 20, mc);
//            order.ChangeDepthTo(0, 20, mc);
//            Assert.IsTrue(order.IsDepthChange, "Should be a depth change");
//            Assert.IsFalse(order.IsSpeedChange, "Should not be a speed change");
//            Assert.IsFalse(order.IsCourseChange, "should not be a course change");

            Movement m2 = m.NextMovement(t, mc, order);
            Assert.IsTrue(m2.IsChanging, "mid-change");
            Assert.AreEqual(11, m2.Depth, "should be 11 meters deep");

            var order2 = new StaticDepthChangeUpOrder(0, 7, mc);
//            order2.ChangeDepthTo(0, 7, mc);
            Movement m3 = m.NextMovement(t2, mc, order2);
            Assert.IsFalse(m3.IsChanging, "completed movement");
            Assert.AreEqual(7, m3.Depth, "should have reached ordered depth");
        }
        [Ignore]
        [TestMethod]
        public void Movement_shouldReturnUpdatedMovement_GivenRelativeDepthChangeOrder()
        {
            //var m = new Movement(0, 0, 10);
            //var t = new TimeSpan(0, 0, 1);
            //var t2 = new TimeSpan(0, 0, 5);
            //var mc = new MovementCharacteristics {DepthChangeRate = 1, MaxDepth = 20, MinDepth = 0};

            //var order = new MovementOrder();
            //order.ChangeDepthRelative(0, ChangeType.Increase, 5, m.Depth, mc);
            //Assert.IsTrue(order.IsDepthChange, "Should be a depth change");
            //Assert.IsFalse(order.IsSpeedChange, "Should not be a speed change");
            //Assert.IsFalse(order.IsCourseChange, "should not be a course change");

            //Movement m2 = m.NextMovement(t, mc, order);
            //Assert.IsTrue(m2.IsChanging, "mid-change");
            //Assert.AreEqual(11, m2.Depth, "should be 11 meters deep");

            //var order2 = new MovementOrder();
            //order2.ChangeDepthRelative(0, ChangeType.Decrease, 5, m.Depth, mc);
            //Movement m3 = m.NextMovement(t2, mc, order2);
            //Assert.IsFalse(m3.IsChanging, "complete movement");
            //Assert.AreEqual(5, m3.Depth, "should have decreased 5");
        }

        [TestMethod]
        public void Movement_ShouldHaveAlwaysHaveAValidCourse()
        {
            //valid course is 0-359, wraps
            //TODO: valid speed is set by MoveMentCharacteristics speed limits assigned to the vesselType and should be tested elsewhere
            //TODO: valid depth is set by MovementCharacteristics depth limits assigned to the vesselType and should be tested elsewhere
            const double course = 50;
            const double speed = 15;
            const double depth = 400;
            var m = new Movement(course, speed, depth);
            Assert.AreEqual(50, m.Course);

            var m1 = new Movement(-20, speed, depth);
            Assert.AreEqual(340, m1.Course);

            var m2 = new Movement(380, speed, depth);
            Assert.AreEqual(20, m2.Course);

            var m3 = new Movement(-1, speed, depth);
            Assert.AreEqual(359, m3.Course);

            var m4 = new Movement(725, speed, depth);
            Assert.AreEqual(5, m4.Course);

            var m5 = new Movement(-725, speed, depth);
            Assert.AreEqual(355, m5.Course);
        }

        [TestMethod]
        public void Movement_ShouldBeAbleToCalculateDistanceTraveledGivenTime()
        {
            const double course = 0;
            const double speed = 10; //knots
            const double depth = 0;
            var m = new Movement(course, speed, depth);
            var time = new TimeSpan(1, 0, 0); //1 hour
            const double expectedDistanceInNauticalMiles = 10;
            const double expectedDistanceInMeters = 18520;

            double result = m.GetDistanceTraveledNm(time);
            Assert.AreEqual(expectedDistanceInNauticalMiles, result);

            result = m.GetDistanceTraveledMeters(time);
            Assert.AreEqual(expectedDistanceInMeters, result);
        }
    }
}