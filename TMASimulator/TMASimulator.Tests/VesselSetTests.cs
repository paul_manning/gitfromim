﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TMASimulator.Models;

namespace TMASimulator.Tests
{
    [TestClass]
    public class VesselSetTests
    {
        [TestMethod]
        public void TMASimulationRun_ShouldBeAbleToAddVessel()
        {
            //Get a reference to the simulation
            var tsr = new VesselSet();

            //Create a new vessel instance
            var testVessel = new Vessel(1);

            //Add the vessel to the simulation run
            tsr.AddVessel(testVessel);
            //test the vessels count increased by 1
            Assert.AreEqual(tsr.GetVesselCount(), 1, "Vessel not added correctly to the simulation");
        }

        [TestMethod]
        public void TMASimulationRun_ShouldBeAbleToRemoveVessel()
        {
            //Get a reference to the solution
            var tsr = new VesselSet();
            //Create 2 new vessels and add them
            var v1 = new Vessel(1);
            var v2 = new Vessel(2);

            //add the vessels to the SimulationRun
            tsr.AddVessel(v1);
            tsr.AddVessel(v2);

            //verify the count is 2
            Assert.AreEqual(tsr.GetVesselCount(), 2, "Vessel set up not adding vessels");
            //RemoveByIndex one    
            tsr.RemoveVessel(v1.VesselId);
            //verify the count is 1
            Assert.AreEqual(tsr.GetVesselCount(), 1, "Vessel count should be 1, but was 2.  RemoveVessels not working");
        }

        [TestMethod]
        public void TMASimulationRun_ShouldBeAbleToListVessels()
        {
            //Assert.Inconclusive("Not Implemented Yet");
            var tsr = new VesselSet();
            const int numVesselsToAdd = 5;
            FillSampleVessels(numVesselsToAdd, tsr);
            List<Vessel> vesselList = tsr.GetAllVessels();
            int iCounter = 0;
            foreach (Vessel theVessel in vesselList)
            {
                Assert.AreEqual(theVessel.VesselId, iCounter, "GetAllVessels should return vessels in the order added");
                iCounter++;
            }
            Assert.AreEqual(numVesselsToAdd, vesselList.Count, "GetAllVessels count mismatch");
        }

        [TestMethod]
        public void TMASimulationRun_ShouldBeAbleToReplaceVessel()
        {
            //Create run
            var tsr = new VesselSet();
            const int numVesselsToAdd = 6;
            const int vesselToReplaceIndex = 1;
            const int vesselIndexReplacementId = 99;
            FillSampleVessels(numVesselsToAdd, tsr);

            //create new vessel with id 99
            var newVessel = new Vessel(vesselIndexReplacementId);
            //replace #2 with new vessel

            tsr.ReplaceVessel(vesselToReplaceIndex, newVessel);
            //verify count
            Assert.AreEqual(tsr.GetVesselCount(), numVesselsToAdd, "Vessel Count Mismatch after replacement");
            //verify that second item has id = 99
            Assert.AreEqual(vesselIndexReplacementId, tsr.GetVesselByIndex(vesselToReplaceIndex).VesselId,
                "Vessel returned incorrect ID");


            //Assert.Inconclusive("Not Implemented Yet");
        }

        [TestMethod]
        public void TMASimulationRun_ShouldBeAbleToGetOneVesselById()
        {
            //setup
            var tsr = new VesselSet();
            const int numVesselsToAdd = 5;
            const int vesselIdToGet = 3;
            FillSampleVessels(numVesselsToAdd, tsr);


            //get one vessel
            Vessel resultVessel = tsr.GetVesselById(vesselIdToGet);

            //test its VesselId Matches the expected item
            Assert.AreEqual(vesselIdToGet, resultVessel.VesselId, "Unexpected id on vessel returned");
        }

        [TestMethod]
        [ExpectedException(typeof (KeyNotFoundException))]
        public void TMASimulationRun_ShouldThrowExceptionIfTryingToReturnAVesselWithAnInvalidID()
        {
            //setup
            var tsr = new VesselSet();
            const int numVesselsToAdd = 5;
            FillSampleVessels(numVesselsToAdd, tsr);

            //find by an invalid id to cause exception
            // ReSharper disable once UnusedVariable
            Vessel failedVessel = tsr.GetVesselById(99);
            Assert.Fail();
        }

        [TestMethod]
        public void TMASimulationRun_ShouldBeAbleToGetOneVesselByIndex()
        {
            var tsr = new VesselSet();
            const int numVesselsToAdd = 5;
            const int vesselIndexToGet = 4;
            const int expectedResult = 4;

            FillSampleVessels(numVesselsToAdd, tsr);

            Assert.AreEqual(expectedResult, tsr.GetVesselByIndex(vesselIndexToGet).VesselId, "Vessel Index Mismatch");
        }

        private static void FillSampleVessels(int numVesselsToAdd, VesselSet tsr)
        {
            for (int vid = 0; vid < numVesselsToAdd; vid++)
            {
                tsr.AddVessel(new Vessel(vid));
            }
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void TMASimulationRun_ShouldThrowExceptionIfTryingToReturnAVesselWithAnInvalidIndex()
        {
            var tsr = new VesselSet();
            const int numVesselsToAdd = 5;
            FillSampleVessels(numVesselsToAdd, tsr);
            const int vessselIndexToGet = 99;

            // ReSharper disable once UnusedVariable
            Vessel failedVessel = tsr.GetVesselByIndex(vessselIndexToGet);

            Assert.Fail();
        }

        [TestMethod]
        public void TMASimulationRun_ShouldBeAbleToReorderVessels()
        {
            //setup add 5 items, expect order to be [0, 1, 2, 3, 4]
            var tsr = new VesselSet();
            const int numVesselsToAdd = 5;
            const int vesselToMove = 1;
            const int positionToMoveTo = 3;
            int[] expectedOrder = {0, 2, 3, 1, 4};
            FillSampleVessels(numVesselsToAdd, tsr);

            //move item 2 to position 4, expect order to be [0, 2, 3, 1, 4]
            tsr.MoveVesselItem(vesselToMove, positionToMoveTo);
            for (int vindex = 0; vindex < numVesselsToAdd; vindex++)
            {
                Assert.AreEqual(expectedOrder[vindex], tsr.GetVesselByIndex(vindex).VesselId,
                    "Unexpected Ordering after changing vessel order");
            }
        }

        [TestMethod]
        public void Vessel_ShouldBeAbleToAddAPosition()
        {
            //setup
            const double lat = 5;
            const double lon = -10;
            var v = new Vessel(1);
            v.Position = new Position(lat, lon);
            Assert.AreEqual(true, v.Position.IsValid());
        }
    }
}